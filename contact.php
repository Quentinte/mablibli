<?php 
include('models/model.php'); 
include('class/User.php'); 
include('class/Book.php'); 
include('class/Order.php'); 
session_start(); 

	if(isset($_SESSION['user']))
	{
		$user = $_SESSION['user'];
	}
	else
	{	
		$user = new User(); 
	}

	if(isset($_SESSION['order']))
	{
		$order = $_SESSION['order'];
	}
	else
	{
		$order = new Order();
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Mablibli : livres en ligne !</title>
	<link rel="stylesheet" type="text/css" href="views/css/index.css">
	<link rel="stylesheet" type="text/css" href="views/css/contact.css">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
</head>
<body>

	<content>

		<header>
			<h1><a href="index.php">Mablibli</a></h1>

			<table>
				<tr>
					<form>
					<td><input id="searchText" type="text"></td><td><input id="searchBtn" type="submit" value="Rechercher"></td>
					</form>
				</tr>
			</table>
			
			<nav>

				<?php include('views/include/menu.php'); ?>

			</nav>
		</header>

		<main>

			<article>
				<p><b>MaBlibli</b><br>
					4 Rue Saint-Charles<br>
					57000 METZ<br>
				</p>
			</article>

			<article>
				<form method="mail">
				<table>
					<tr><td><input type="email" name="mail" placeholder="Adresse mail"></td></tr>
					<tr><td><input type="text" name="object" placeholder="Objet"></td></tr>
					<tr><td><textarea name="message" placeholder="Votre message" rows="10" cols="50"></textarea></td></tr>
					<tr><td><input type="submit" name="SUBMIT_CONTACT" value="Envoyer"></td></tr>
				</table>
				</form>
			</article>
		</main>

		<footer></footer>

	<content>

</body>
</html>
