<?php

function dbConnect()
{
	try
	{
		$db = new PDO('mysql:host=localhost;dbname=mablibli;charset=utf8', 'root', 'root', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		return $db;
	}
	catch (Exception $e)
	{
	        die('Erreur : ' . $e->getMessage());
	}
}

function countBooks()
{
	$db = dbConnect();

	$req = $db->query("
		SELECT count(id) AS countID
		FROM books
		");

		while ($count = $req->fetch())
		{
			$totalCount = $count['countID'];
		}

		return $totalCount;

	$req->closeCursor();
}

function showOrders($user)
{
	$db = dbConnect();
	$userID = $user->getId();

	$req = $db->prepare('SELECT id, price, register_date FROM orders WHERE userID = :userID ORDER BY id DESC');
	$req->execute(array(
		'userID' => $userID
		));
	return $req;

	$req->closeCursor();
}

function showOrderDetails($id)
{
	$db = dbConnect();

	$req = $db->prepare('SELECT ordersdetails.quantity AS quantity, books.name AS name FROM ordersdetails INNER JOIN books ON books.id = ordersdetails.bookID WHERE ordersdetails.orderID = :id');
	$req->execute(array(
		'id' => $id
		));
	return $req;

	$req->closeCursor();
}

function showBooks($genre, $method = 'Simple')
{
	$db = dbConnect();

	$genre = htmlspecialchars($genre);
	$method = htmlspecialchars($method);

	if($genre == 'Roman' OR $genre == 'Bande dessinée')
	{


		if($method === 'Simple')
		{	
			$req = $db->prepare('SELECT * FROM books WHERE genre = :genre ORDER BY date ASC');
			$req->execute(array(
			':genre' => $genre
			));

			return $req;

			$req->closeCursor();
		}
		elseif($method === 'Price')
		{
			$req = $db->prepare('SELECT * FROM books WHERE genre = :genre ORDER BY price ASC');
			$req->execute(array(
			':genre' => 'Roman'
			));

			return $req;

			$req->closeCursor();
		}
		elseif($method === 'Name')
		{
			$req = $db->prepare('SELECT * FROM books WHERE genre = :genre ORDER BY name ASC');
			$req->execute(array(
			':genre' => $genre
			));

			return $req;

			$req->closeCursor();
		}

	}
	else
	{

		if($method === 'Simple')
		{	
			$req = $db->query('SELECT * FROM books ORDER BY date ASC');

			return $req;

			$req->closeCursor();
		}
		elseif($method === 'Price')
		{
			$req = $db->query('SELECT * FROM books ORDER BY price ASC');

			return $req;

			$req->closeCursor();
		}
		elseif($method === 'Name')
		{
			$req = $db->query('SELECT * FROM books ORDER BY name ASC');

			return $req;

			$req->closeCursor();
		}

	}



	// 	if($method === 'Simple')
	// {	
	// 	$req = $db->prepare('SELECT books.*, authors.lastname AS lastname, authors.firstname AS firstname FROM books INNER JOIN booksAuthors ON booksAuthors.bookID = books.id INNER JOIN authors ON booksAuthors.authorID = authors.id WHERE books.id = :id');
	// 	$req->execute(array(
	// 		'id' => $i));

	// 	return $data = $req->fetch();

	// 	$req->closeCursor();
	// }

		// A METTRE POUR AFFICHER TOUS LES LIVRES
	// $getBooks = showBooks();

					// while($books = $getBooks->fetch())
					// {
					// 	echo $books['id'] . ' ' . $books['name'];
					// }
}

