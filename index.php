<?php 
include('models/model.php'); 
include('class/User.php'); 
include('class/Book.php'); 
include('class/Order.php'); 
session_start(); 

	if(isset($_SESSION['user']))
	{
		$user = $_SESSION['user'];
	}
	else
	{	
		$user = new User(); 
	}

	if(isset($_SESSION['order']))
	{
		$order = $_SESSION['order'];
	}
	else
	{
		$order = new Order();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Mablibli : livres en ligne !</title>
	<link rel="stylesheet" type="text/css" href="views/css/index.css">
	<link rel="stylesheet" type="text/css" href="views/css/home.css">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
</head>
<body>

	<content>

		<header>
			<h1><a href="index.php">Mablibli</a></h1>

			<table>
				<tr>
					<form>
					<td><input id="searchText" type="text"></td><td><input id="searchBtn" type="submit" value="Rechercher"></td>
					</form>
				</tr>
			</table>
			
			<nav>

				<?php include('views/include/menu.php'); ?>

			</nav>
		</header>
<?php
	$book1 = new Book(3);
	$book2 = new Book(2);
	$book3 = new Book(4);
?>



		<main>
			<article>
				<h3>Best Seller</h3>
			</article>

			<article>
			<aside>
				<?php

				echo '<a href="book.php?id=' .$book1->getId() . '"><img src="public/img/books/' . $book1->getImage() . '" title="' . $book1->getName() . '"></a>';
	
				?>
			</aside>
			<aside>
				<?php

				echo '<a href="book.php?id=' .$book2->getId() . '"><img src="public/img/books/' . $book2->getImage() . '" title="' . $book2->getName() . '"></a>';
	
				?>
			</aside>
			<aside>
				<?php

				echo '<a href="book.php?id=' .$book3->getId() . '"><img src="public/img/books/' . $book3->getImage() . '" title="' . $book3->getName() . '"></a>';
	
				?>
			</aside>
			</article>
		</main>

		<footer></footer>

	<content>

</body>
</html>