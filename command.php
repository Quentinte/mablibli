<?php 
include('models/model.php'); 
include('class/User.php'); 
include('class/Book.php'); 
include('class/Order.php'); 
session_start(); 

	if(isset($_SESSION['user']))
	{
		$user = $_SESSION['user'];
	}
	else
	{	
		$user = new User(); 
	}

	if(isset($_SESSION['order']))
	{
		$order = $_SESSION['order'];
	}
	else
	{
		$order = new Order();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Mablibli : livres en ligne !</title>
	<link rel="stylesheet" type="text/css" href="views/css/index.css">
	<link rel="stylesheet" type="text/css" href="views/css/home.css">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
</head>
<body>

	<content>

		<header>
			<h1><a href="index.php">Mablibli</a></h1>

			<table>
				<tr>
					<form>
					<td><input id="searchText" type="text"></td><td><input id="searchBtn" type="submit" value="Rechercher"></td>
					</form>
				</tr>
			</table>
			
			<nav>

				<?php include('views/include/menu.php'); ?>

			</nav>
		</header>



		<main>
			<article>
				<?php
				if($user->getConnect() > 0)
				{
					if($order->nbBooks() > 0)
					{
						if(isset($_POST['SUBMIT_ORDER']))
						{
							echo $order->validOrder($user);
							echo 'Votre commande a bien été validée.';
						}
						else
						{
				?>
					
					<form method="POST" action="command.php">
					<p><b>Votre adresse de livraison:</b> <br>
					<?= $user->getAddress() ?></p>

					<p><b>Choisissez votre moyen de paiement:</b><br>
						<input type="radio" name="pay" value="CB" id="CB"/><label for="CB">Carte Bancaire</label>
						<input type="radio" name="pay" value="Chèque" id="CHQ"/><label for="CHQ">Chèque</label>
						<input type="radio" name="pay" value="Paypal" id="PP"/><label for="PP">Paypal</label>
					</p>

					<p><input type="submit" name="SUBMIT_ORDER" value="Commander"></p>
					</form>

				<?php
						}
					}
					else
					{
						echo 'Votre panier est vide.';
					}
				}
				else
				{
					header('Location: compte.php');
					exit();
				}
				?>
			</article>

			<article>

			<aside>
			</aside>

			<aside>
			</aside>

			<aside>
			</aside>
			
			</article>
		</main>

		<footer></footer>

	<content>

</body>
</html>