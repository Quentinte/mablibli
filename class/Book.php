<?php

class Book
{
	protected $id;
	protected $name;
	protected $image;
	protected $description;
	protected $price;
	protected $genre;
	protected $date;
	protected $author;

	public function __construct($id)
	{
		$id = strip_tags($id);
		$db = dbConnect();

		$req = $db->prepare('SELECT books.*, authors.lastname AS lastname, authors.firstname AS firstname FROM books INNER JOIN booksAuthors ON booksAuthors.bookID = books.id INNER JOIN authors ON booksAuthors.authorID = authors.id WHERE books.id = :id');
		$req->execute(array(
			'id' => $id));
		$data = $req->fetch();

		$this->id = $data['id'];
		$this->name = $data['name'];
		$this->image = $data['img'];
		$this->description = $data['description'];
		$this->price = $data['price'];
		$this->gente = $data['genre'];
		$this->date = $data['date'];
		$this->author = $data['firstname'] . ' ' . $data['lastname'];
	}

	public function getId()
	{
		return $this->id;
	}

	public function getAuthors()
	{
		return $this->author;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getImage()
	{
		return $this->image;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getGenre()
	{
		return $this->genre;
	}

	public function getDate()
	{
		return $this->date;
	}
}