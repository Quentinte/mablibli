<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Order
{
	protected $id;
	protected $listBooks = Array(); // doit contenir objet (livre) + combien il en veut
	protected $price;
	protected $date;
	protected $user;

	public function __construct()
	{
		// $this->setPrice();
	}

	public function setUser($user)
	{
		$this->user = $user;
	}

	public function nbBooks()
	{
		$i = 0;

		foreach ($this->listBooks as $key) {
			$i++;
		}

		return $i;
	}

	public function addBook($book, $quantity = 1)
	{
		$i = $this->nbBooks();
		$exist = -1;

		for ($b=0; $b < $i; $b++) { 
			$booksExist = $this->listBooks[$b][0];

			if($booksExist == $book)
			{
				$exist = $b;
				break;
			}
		}

		if($exist > -1)
		{
			$this->listBooks[$exist][1] += $quantity;
			$this->setPrice();
		}
		else
		{
			$this->listBooks[$i] = array($book, $quantity);
			$this->setPrice();
		}

	}

	public function getBooks()
	{
		return $this->listBooks;
	}

	public function getBook($i)
	{
		return $this->listBooks[$i][0];
	}

	public function deleteBook($i)
	{
		array_splice($this->listBooks, $i, 1);
		$this->setPrice();		
	}

	public function getQuantity($i)
	{
		return $this->listBooks[$i][1];
	}

	public function upQuantity($i)
	{
		$this->listBooks[$i][1]+=1;
		$this->setPrice();
	}

	public function downQuantity($i)
	{
		$this->listBooks[$i][1]-=1;
		$this->setPrice();
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function setPrice()
	{
		$price = 0;
		$objet = '';
		$i = $this->nbBooks();

		for($b = 0; $b < $i; $b++)
		{
			$objet = $this->listBooks[$b][0];
			$quantity = $this->listBooks[$b][1];

			$price += ($objet->getPrice() * $quantity);
		}

		$this->price = $price;
	}

	public function destroyOrder()
	{
		$this->listBooks = Array();
		$this->price = 0;
	}

	public function validOrder($user)
	{
		$db = dbConnect();
		$user = $user->getId();

		$req = $db->prepare('INSERT INTO orders(userID, price, register_date) VALUES(:user, :price, NOW())');
		$req->execute(array(
			'user' => $user,
			'price' => $this->getPrice()
			));
		$req->closeCursor();

		$req = $db->prepare('SELECT id FROM orders WHERE userID = :user ORDER BY id DESC LIMIT 0,1');
		$req->execute(array(
			'user' => $user
			));

		$data = $req->fetch();

		$id = $data['id'];
		$this->id = $id;

		$req->closeCursor();

		for ($i=0; $i < $this->nbBooks(); $i++) { 
			$idBook = $this->getBook($i)->getId();
			$quantity = $this->getQuantity($i);

			$req = $db->prepare('INSERT INTO ordersdetails(bookID, orderID, quantity) VALUES(:bookID, :orderID, :quantity)');
			$req->execute(array(
				'bookID' => $idBook,
				'orderID' => $id,
				'quantity' => $quantity
				));
			$req->closeCursor();		
		}
		
		// Envoi email

		require 'mail/PHPMailer/Exception.php';
		require 'mail/PHPMailer/PHPMailer.php';
		require 'mail/PHPMailer/SMTP.php';

		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->Host = "SSL0.OVH.NET";
		$mail->SMTPAuth = true;
		$mail->Username = "adressemail@stagiairesifa.fr";
		$mail->Password = "*******";
		$mail->SMTPSecure = "tls";
		$mail->Port = 587;

		$mail->setFrom('terver.quentin@stagiairesifa.fr', "MaBlibli");
		$mail->addAddress("terver.quentin@stagiairesifa.fr");

		$mail->isHTML(true);
		$mail->Subject = "Votre commande numero $this->id ";
		$mail->Body = "

		<h1>Votre commande a bien ete validee !</h1><br>
		
		Votre commande d'un montant de <b>$this->price €</b> a bien ete validee.<br>
		Pour retrouver le detail de votre commandez, rendez vous dans votre espace client.
		";

		$maile = $mail->send();


		$this->destroyOrder();	

	}


}