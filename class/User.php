<?php

class User
{
	protected $id;
	protected $pseudo;
	protected $mail;
	protected $password;
	protected $register_date;
	protected $address;
	protected $connect = 0;

	public function getId()
	{
		return $this->id;
	}
	
	public function getConnect()
	{
		return $this->connect;
	}

	public function getName()
	{
		return $this->pseudo;
	}

	public function getAddress()
	{
		return $this->address;
	}

	public function getMail()
	{
		return $this->mail;
	}

	public function getDate()
	{
		return $this->register_date;
	}

	public function setProfil($mail, $address)
	{
		$db = dbConnect();

		if(preg_match("#^[a-z0-9]+[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $mail))
		{
			$mail_valid = 1;
		}
		else
		{
			$mail_valid = 0;
		}

		$address = htmlspecialchars($address);
		$autorisation_modif = 0;
		$verif_mail = 1;
		$etat = '';

		if($mail_valid == 1)
		{
			$req = $db->prepare('SELECT mail FROM users WHERE id = :id');
					$req->execute(array(
						'id' => $this->id
					));
			while($mail_user = $req->fetch())
			{
				if($mail_user['mail'] == $mail)
				{
					$verif_mail = 0;
				}
			}
			$req->closeCursor();

			if($verif_mail == 1)
			{

				$req = $db->query('SELECT mail FROM users');
				while ($list_mail = $req->fetch())
				{
					if($list_mail['mail'] === $mail)
					{
						$autorisation_modif = 1;
					}
				}

				$req->closeCursor();
			}

				if($autorisation_modif == 1)
				{
					$etat = "Adresse mail déjà utilisée";
				}
				else
				{
					$req = $db->prepare('UPDATE users SET mail = :mail, address = :address WHERE id = :id');
					$req->execute(array(
						'mail' => $mail,
						'address' => $address,
						'id' => $this->id
					));

					$etat = "Informations modifiés avec succès";
					
					$this->mail = $mail;
					$this->address = $address;
				}
			}
			else
			{
				$etat = "Adresse mail invalide";
			}

			return $etat;
	}

	public function signUp($pseudo, $pass1, $pass2, $mail, $address)
	{
		$pseudo = ucfirst(trim(strip_tags(strtolower($pseudo)), ' '));
		$pass1 = trim(strip_tags($pass1), ' ');
		$pass2 = trim(strip_tags($pass2), ' ');
		$mail = trim(strip_tags(strtolower($mail)), ' ');
		$address = htmlspecialchars($address);
		$error_signUp = '';

		$db = dbConnect();


	if(preg_match("#^[a-z0-9]+[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $mail))
	{
		$mail_valid = 1;
	}
	else
	{
		$mail_valid = 0;
	}

	if(preg_match("#^[a-z]{1}[a-z0-9]{2,12}$#i", $pseudo))
	{
		$pseudo_valid = 1;
	}
	else
	{
		$pseudo_valid = 0;
	}

	$autorisation_ajout = 2;

	if( ($mail_valid === 1) AND ($pass1 === $pass2) AND ($pseudo_valid === 1) )
			{
				$req = $db->query('SELECT pseudo FROM users');
				while ($list_pseudo = $req->fetch())
				{
					if ($list_pseudo['pseudo'] === $pseudo)
					{
						$autorisation_ajout = 0;
						// Le nom existe déjà
					}
				}

				$req->closeCursor();

				$req = $db->query('SELECT mail FROM users');
				while ($list_mail = $req->fetch())
				{
					if($list_mail['mail'] === $mail)
					{
						$autorisation_ajout = 1;
					}
				}

				$req->closeCursor();

				if($autorisation_ajout === 2)
				{
					$pass_hache = password_hash($pass1, PASSWORD_DEFAULT);

					$nbr_activation = rand(1000,9999);

					$req = $db->prepare('INSERT INTO users(pseudo, password, mail, nbr_activation, address, register_date) VALUES(:pseudo, :pass, :mail, :activation_key, :address, NOW())');
					$req->execute(array(
					    'pseudo' => $pseudo,
					    'pass' => strip_tags($pass_hache),
 					    'mail' => $mail,
						'activation_key' => strip_tags($nbr_activation),
						'address' => $address
						));

					$req->closeCursor();

					$error_signUp = 1;
				}


				elseif($autorisation_ajout === 0)
	  			{
	  				$error_signUp = 'Cet identifiant existe déjà';
	  			}
	  			elseif ($autorisation_ajout === 1) {
	  				$error_signUp = 'Cette adresse mail est déjà utilisée.';
	  			}
	  			else
	  			{
	  				$error_signUp = 'Erreur';
	  			}
	  		}

	
	elseif(($mail_valid === 1) AND ($pass1 !== $pass2) AND ($pseudo_valid === 1))
	{
		$error_signUp = 'Les deux mots de passe ne sont pas identiques';
	}
	elseif(($mail_valid === 1) AND ($pass1 === $pass2) AND ($pseudo_valid === 0))
	{
		$error_signUp = '<p>L\'identifiant doit commencer par une lettre et doit contenir entre 3 et 13 caractères.</p>';
	}
	elseif(($mail_valid === 0) AND ($pass1 === $pass2) AND ($pseudo_valid === 1))
	{
		$error_signUp = 'Adresse mail invalide';
	}
	elseif(($mail_valid === 0) AND ($pass1 === $pass2) AND ($pseudo_valid === 0))
	{
		$error_signUp = 'Identifiant et adresse mail invalide';
	}
	else
	{
		$error_signUp = 'Une donnée est manquante ou fausse';
	}

	return $error_signUp;

	}

	public function signIn($pseudo, $password)
	{
		$db = dbConnect();

		$req = $db->prepare('SELECT * FROM users WHERE pseudo = :pseudo');
		$req->execute(array(
			'pseudo' => $pseudo));
		$data = $req->fetch();

		$isPasswordCorrect = password_verify($password, $data['password']);

		if(!$data)
		{
			$enterID = 0;
		}
		else
		{
			if($isPasswordCorrect)
			{
				$enterID = $data['id'];
				$this->id = $data['id'];
				$this->pseudo = $data['pseudo'];
				$this->mail = $data['mail'];
				$this->register_date = $data['register_date'];
				$this->address = $data['address'];
				$this->connect = 1;
				$req->closeCursor();
			}
			else
			{
				$enterID = 0;
			}
		}

		return $enterID;

		$req->closeCursor();
	}

}