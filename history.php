<?php 
include('models/model.php'); 
include('class/User.php'); 
include('class/Book.php'); 
include('class/Order.php'); 
session_start(); 

	if(isset($_SESSION['user']))
	{
		$user = $_SESSION['user'];
	}
	else
	{	
		$user = new User(); 
	}

	if(isset($_SESSION['order']))
	{
		$order = $_SESSION['order'];
	}
	else
	{
		$order = new Order();
	}

	if(isset($_POST['SUBMIT_SIGNUP']))
	{
		if(!empty($_POST['pseudo']) AND !empty($_POST['password1']) AND !empty($_POST['password2']) AND !empty($_POST['mail']))
		{
			$etatUP = $user->signUp($_POST['pseudo'], $_POST['password1'], $_POST['password2'], $_POST['mail']);
		}
		else
		{
			$etatUP = "Un élément est manquant";
		}
	}

	if(isset($_POST['SUBMIT_SIGNIN']))
	{
		if(!empty($_POST['pseudo']) AND !empty($_POST['password']))
		{
			$etatIN = $user->signIn($_POST['pseudo'], $_POST['password']);
			if($etatIN > 0)
			{
				$_SESSION['user'] = $user;
				header('Location: compte.php');
				exit();
			}
		}
		else
		{
			$etatIN = "Un élément est manquant";
		}
	}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Mablibli : livres en ligne !</title>
	<link rel="stylesheet" type="text/css" href="views/css/index.css">
	<?php
	if($user->getConnect() === 0)
	{
	echo '<link rel="stylesheet" type="text/css" href="views/css/compte.css">';
	}
	else
	{
		echo '<link rel="stylesheet" type="text/css" href="views/css/connect.css">';
	}
	?>
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
</head>
<body>

	<content>

		<header>
			<h1><a href="index.php">Mablibli</a></h1>

			<table>
				<tr>
					<form>
					<td><input id="searchText" type="text"></td><td><input id="searchBtn" type="submit" value="Rechercher"></td>
					</form>
				</tr>
			</table>
			
			<nav>

				<?php include('views/include/menu.php'); ?>

			</nav>
		</header>


		<main>
			<?php
				if($user->getConnect() === 0)
				{
			?>
			<article>
				<h3>Connexion</h3>
				<?php 
					if(isset($etatIN))
					{ 
						echo $etatIN;
					} 
					?>
				<table>

				<form method="POST" action="compte.php">
					<tr><td><input class="text" type="text" name="pseudo" placeholder="Identifiant" required></td></tr>
					<tr><td><input class="text" type="password" name="password" placeholder="Mot de passe" required></td></tr>
					<tr><td><center><input class="btn" type="submit" name="SUBMIT_SIGNIN" value="Connexion"></center></td></tr>
				</form>

				</table>
			</article>

			<article>
				<h3>Inscription</h3>
				<?php 
					if(isset($etatUP))
					{ 
						if($etatUP == 1)
						{
							echo 'Inscription réussi';
						}
						else
						{
							echo $etatUP;
						}
					} 
					?>

				<table>

				<form method="POST" action="compte.php">
					<tr><td><input class="text" type="text" name="pseudo" placeholder="Identifiant" required></td></tr>
					<tr><td><input class="text" type="email" name="mail" placeholder="Adresse e-mail" required></td></tr>
					<tr><td><input class="text" type="password" name="password1" placeholder="Mot de passe" required></td></tr>
					<tr><td><input class="text" type="password" name="password2" placeholder="Confirmation" required></td></tr>
					<tr><td><center><input class="btn" type="submit" name="SUBMIT_SIGNUP" value="Inscription"></center></td></tr>
				</form>

				</table>
			</article>
			<?php
				}
				else
				{
				?>
				<article>
					<aside>
						<p>
							<?php
								$orderHistory = showOrders($user);

								while($dataOrder = $orderHistory->fetch())
								{
									echo '<h3>Commande n°' . $dataOrder['id'] . '</h3> <i>' . $dataOrder['register_date'] . '</i><br>';

									$orderDetails = showOrderDetails($dataOrder['id']);

									while($dataDetails = $orderDetails->fetch())
									{
										echo $dataDetails['name'] . ' (' . $dataDetails['quantity'] . ')<br>';
									}

									echo '<br><b>Total:</b> ' . $dataOrder['price'] . '€';
									echo '<hr>';
								}
							?>
						</p>
					</aside>
				</article>

			<?php
				}
			?>
		</main>

		<footer></footer>

	<content>

</body>
</html>