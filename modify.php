<?php 
include('models/model.php'); 
include('class/User.php'); 
include('class/Book.php'); 
include('class/Order.php'); 
session_start(); 

	if(isset($_SESSION['user']))
	{
		$user = $_SESSION['user'];
	}
	else
	{	
		$user = new User(); 
	}

	if(isset($_SESSION['order']))
	{
		$order = $_SESSION['order'];
	}
	else
	{
		$order = new Order();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Mablibli : livres en ligne !</title>
	<link rel="stylesheet" type="text/css" href="views/css/index.css">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
</head>
<body>

	<content>

		<header>
			<h1><a href="index.php">Mablibli</a></h1>

			<table>
				<tr>
					<form>
					<td><input id="searchText" type="text"></td><td><input id="searchBtn" type="submit" value="Rechercher"></td>
					</form>
				</tr>
			</table>
			
			<nav>

				<?php include('views/include/menu.php'); ?>

			</nav>
		</header>



		<main>
			<article>
				<?php
				if($user->getConnect() > 0)
				{
					if(isset($_POST['SUBMIT_MODIFY']) AND !empty($_POST['mail']) AND !empty($_POST['address']))
					{
						$etat = $user->setProfil($_POST['mail'], $_POST['address']);
						echo $etat;
					}
				?>
					<form method="POST">
						<input type="text" name="mail" value="<?= $user->getMail(); ?>"><br>
						<input type="text" name="address" value="<?= $user->getAddress(); ?>"><br>
						<input type="submit" value="Modifier" name="SUBMIT_MODIFY">
					</form> 
				<?php
				}
				else
				{
					header('Location: compte.php');
					exit();
				}
				?>
			</article>

		</main>

		<footer></footer>

	<content>

</body>
</html>