-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  mar. 23 avr. 2019 à 14:42
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mablibli`
--

-- --------------------------------------------------------

--
-- Structure de la table `authors`
--

CREATE TABLE `authors` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `birth_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `authors`
--

INSERT INTO `authors` (`id`, `firstname`, `lastname`, `birth_date`) VALUES
(1, 'Jean-Christophe', 'Grangé', '1961-07-15'),
(2, 'Camilla', 'Läckberg', '1974-08-30'),
(3, 'Dov', 'Alfon', '1961-04-24'),
(4, 'Enki', 'Bilal', '1951-10-07'),
(5, 'Marc', 'Lévy', '1961-10-16'),
(6, 'Marie-Bernadette', 'Dupuy', '1952-10-30'),
(7, 'Victor', 'Hugo', '1885-05-22'),
(8, 'Guillaume', 'Musso', '1974-07-20');

-- --------------------------------------------------------

--
-- Structure de la table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `price` int(11) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `books`
--

INSERT INTO `books` (`id`, `name`, `img`, `date`, `price`, `genre`, `description`) VALUES
(1, 'La Dernière Chasse', 'ladernierechasse.jpg', '2019-04-10', 23, 'Roman', 'En Forêt noire, la dernière chasse a commencé ...\r\n\r\nEt quand l\'hallali sonnera, la bête immonde ne sera pas celle qu\'on croit.'),
(2, 'La cage dorée', 'lacagedoree.jpg', '2019-04-25', 23, 'Roman', 'Le talent littéraire de Camilla Läckberg n\'est plus à prouver. Il s\'illustre une nouvelle fois dans La Cage dorée, un polar glaçant. Dans les hautes sphères suédoises, Jack quitte Faye pour sa collaboratrice. Cela ne manque pas de réveiller les pulsions les plus perverses de Faye. Celle-ci se transformera en furie. Cruelle, elle n’hésitera pas à lui faire vivre le pire pour se venger de l’adultère commis. Un thriller à glacer le sang, qui explore toutes les dimensions les plus malsaines d’un esprit humain.'),
(3, 'Unité 8200', 'unite8200.jpg', '2019-04-11', 21, 'Roman', 'A son arrivée à Roissy, un responsable marketing israélien est enlevé par une hôtesse blonde. Alerté, le colonel Zeev Abadi, officier des renseignements israéliens, assiste le commissaire Léger de la police judiciaire. Quand le corps du voyageur est retrouvé dans une usine de traitement des déchets et que ses kidnappeurs sont éliminés par drone en plein Paris, l\'affaire devient une traque.'),
(4, 'Bug', 'bug.jpg', '2019-04-17', 18, 'Bande dessinée', 'Un album de science-fiction des plus haletants : Enki Bilal poursuit sur sa lancée avec le deuxième volume de Bug, sa série-événement. Dans le premier opus, le monde numérique disparaissait, et le héros se retrouvait seul. Il est dorénavant convoité par tous, en raison de sa mémoire intacte. À la recherche de sa fille, il tentera de survivre dans un monde hostile. Bug, Volume 2 est une bande dessinée au graphisme précis qui aborde non sans humour notre addiction aux technologies. À ne manquer sous aucun prétexte.'),
(5, 'Ghost in Love', 'ghostinlove.jpg', '2019-05-14', 22, 'Roman', 'Ils ont trois jours à San Francisco.\r\nTrois jours pour écrire leur histoire.Que feriez-vous si un fantôme débarquait dans votre vie et vous demandait de l\'aider à réaliser son voeu le plus cher ?\r\nSeriez-vous prêt à partir avec lui en avion à l\'autre bout du monde ? Au risque de passer pour un fou ?\r\n\r\n\r\nEt si ce fantôme était celui de votre père ?\r\n\r\n\r\nThomas, pianiste virtuose, est entraîné dans une aventure fabuleuse : une promesse, un voyage pour rattraper le temps perdu, et une rencontre inattendue...'),
(6, 'Les lumières de Broadway', 'leslumieresdebroadway.jpg', '2019-05-09', 23, 'Roman', 'À Paris, une jeune femme se jette dans la Seine du haut du Pont-Neuf. Tourmentée par son passé, Élisabeth ne peut plus vivre avec son terrible secret. Quelques semaines auparavant, elle a fui le château des Laroche, sa famille\r\nbiologique, en Charente, pour se réfugier dans la capitale. Sauvée par les mariniers, Élisabeth se livre enfin à son fiancé et lui raconte l’intolérable vérité.'),
(7, 'Les Misérables', 'lesmiserables.jpg', '2014-08-01', 5, 'Roman', 'Le destin de Jean Valjean, forçat échappé du bagne, est bouleversé par sa rencontre avec Fantine. Mourante et sans le sou, celle-ci lui demande de prendre soin de Cosette, sa fille confiée aux Thénardier. Ce couple d’aubergistes, malhonnête et sans scrupules, exploitent la fillette jusqu’à ce que Jean Valjean tienne sa promesse et l’adopte. Cosette devient alors sa raison de vivre. Mais son passé le rattrape et l’inspecteur Javert le traque…'),
(8, 'La jeune fille et la nuit', 'lajeunefilleetlanuit.jpg', '2019-03-20', 23, 'Roman', 'Un campus prestigieux figé sous la neige\r\nTrois amis liés par un secret tragique\r\nUne jeune fille emportée par la nuit\r\n\r\nCôte d’Azur - Hiver 1992\r\nUne nuit glaciale, alors que le campus de son lycée est paralysé par une tempête de neige, Vinca Rockwell, 19 ans, s’enfuit avec son professeur de philo avec qui elle entretenait une relation secrète. Personne ne la reverra jamais.');

-- --------------------------------------------------------

--
-- Structure de la table `booksauthors`
--

CREATE TABLE `booksauthors` (
  `bookID` int(11) NOT NULL,
  `authorID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `booksauthors`
--

INSERT INTO `booksauthors` (`bookID`, `authorID`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(8, 8),
(7, 7);

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `register_date` datetime NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ordersdetails`
--

CREATE TABLE `ordersdetails` (
  `bookID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `register_date` date NOT NULL,
  `mail` varchar(255) NOT NULL,
  `nbr_activation` int(11) NOT NULL,
  `address` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `booksauthors`
--
ALTER TABLE `booksauthors`
  ADD KEY `authorID` (`authorID`),
  ADD KEY `IDbook` (`bookID`);

--
-- Index pour la table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userID` (`userID`);

--
-- Index pour la table `ordersdetails`
--
ALTER TABLE `ordersdetails`
  ADD KEY `orderID` (`orderID`),
  ADD KEY `bookID` (`bookID`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `booksauthors`
--
ALTER TABLE `booksauthors`
  ADD CONSTRAINT `IDbook` FOREIGN KEY (`bookID`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `authorID` FOREIGN KEY (`authorID`) REFERENCES `authors` (`id`);

--
-- Contraintes pour la table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `userID` FOREIGN KEY (`userID`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `ordersdetails`
--
ALTER TABLE `ordersdetails`
  ADD CONSTRAINT `bookID` FOREIGN KEY (`bookID`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `orderID` FOREIGN KEY (`orderID`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
