<?php 
include('models/model.php'); 
include('class/User.php'); 
include('class/Book.php'); 
include('class/Order.php'); 
session_start(); 

	if(isset($_SESSION['user']))
	{
		$user = $_SESSION['user'];
	}
	else
	{	
		$user = new User(); 
	}

	if(isset($_SESSION['order']))
	{
		$order = $_SESSION['order'];
	}
	else
	{
		$order = new Order();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Mablibli : livres en ligne !</title>
	<link rel="stylesheet" type="text/css" href="views/css/index.css">
	<link rel="stylesheet" type="text/css" href="views/css/home.css">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
</head>
<body>

	<content>

		<header>
			<h1><a href="index.php">Mablibli</a></h1>

			<table>
				<tr>
					<form>
					<td><input id="searchText" type="text"></td><td><input id="searchBtn" type="submit" value="Rechercher"></td>
					</form>
				</tr>
			</table>
			
			<nav>

				<?php include('views/include/menu.php'); ?>

			</nav>
		</header>



		<main>
			<article>
				<?php
				if($order->nbBooks() > 0)
				{

				if(isset($_POST['SUBMIT_UP']))
				{
					$order->upQuantity($_POST['idChange']);
				}
				elseif(isset($_POST['SUBMIT_DOWN']))
				{
					$order->downQuantity($_POST['idChange']);
				}
				elseif(isset($_POST['SUBMIT_DELETE']))
				{
					$order->deleteBook($_POST['idChange']);
				}

					for ($i=0; $i < $order->nbBooks(); $i++) { 
						$book = $order->getBook($i);

						echo '<form method="POST" action="panier.php">';
						echo $book->getName() .
							'<input type="hidden" name="idChange" value="'.$i.'">
							<input type="submit"';

							if($order->getQuantity($i) > 1){ echo 'name="SUBMIT_DOWN"'; }

						echo 'value="-">'. $order->getQuantity($i) . '<input type="submit" name="SUBMIT_UP" value="+"><input type="submit" name="SUBMIT_DELETE" value="X">';
						echo '</form><br>';
					}

					echo 'Prix: ' . $order->getPrice() . '€<br>';

					if($user->getConnect() == 1)
					{
						echo '<a href="command.php">Commander</a>';
					}
					else
					{
						echo '<a href="compte.php">Commander</a>';
					}
				?>

				<a href="destroy_order.php">Vider le panier</a>

				<?php
				}
				else
				{
					echo 'Votre panier est vide.';
				}
				?>
			</article>

			<article>

			<aside>
			</aside>

			<aside>
			</aside>

			<aside>
			</aside>
			
			</article>
		</main>

		<footer></footer>

	<content>

</body>
</html>