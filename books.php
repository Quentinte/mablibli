<?php 
include('models/model.php'); 
include('class/User.php'); 
include('class/Book.php'); 
include('class/Order.php'); 
session_start(); 

	if(isset($_SESSION['user']))
	{
		$user = $_SESSION['user'];
	}
	else
	{	
		$user = new User(); 
	}

	if(isset($_SESSION['order']))
	{
		$order = $_SESSION['order'];
	}
	else
	{
		$order = new Order();
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Mablibli : livres en ligne !</title>
	<link rel="stylesheet" type="text/css" href="views/css/index.css">
	<link rel="stylesheet" type="text/css" href="views/css/books.css">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
</head>
<body>

	<content>

		<header>
			<h1><a href="index.php">Mablibli</a></h1>

			<table>
				<tr>
					<form>
					<td><input id="searchText" type="text"></td><td><input id="searchBtn" type="submit" value="Rechercher"></td>
					</form>
				</tr>
			</table>
			
			<nav>

				<?php include('views/include/menu.php'); ?>

			</nav>
		</header>
<?php
	$countBooks = countBooks();

?>



		<main>
			<article>
				<table>
					<tr><td>Trier par:</td><td><a href="?
						<?php 
						if(isset($_GET['cat'])){
						 echo 'cat='. $_GET['cat'] . '&'; }  
						?>
					sort=Price">Prix</a></td><td>|</td><td><a href="?<?php 
						if(isset($_GET['cat'])){
						 echo 'cat='. $_GET['cat'] . '&'; }  
						?>sort=Date">Date de parution</a></td><td> |</td><td><a href="?<?php 
						if(isset($_GET['cat'])){
						 echo 'cat='. $_GET['cat'] . '&'; }  
						?>sort=Name">Nom</a></td></tr>
					<tr><td>Catégories:</td><td><a href="?cat=Roman">Roman</a></td><td>|</td><td><a href="?cat=BD">BD</a></td><td>
				</table>
			</article>
			<article>

				<?php 
				if(isset($_GET['cat']) AND $_GET['cat'] === 'Roman')
				{
					$genre = 'Roman';
				}
				elseif(isset($_GET['cat']) AND $_GET['cat'] === 'BD')
				{
					$genre = 'Bande dessinée';
				}
				else
				{
					$genre = 'All';
				}

				if(isset($_GET['sort']) AND $_GET['sort'] === 'Price')
				{
					$getBooks = showBooks($genre, 'Price');
				}
				elseif(isset($_GET['sort']) AND $_GET['sort'] === 'Name')
				{
					$getBooks = showBooks($genre, 'Name');
				}
				else
				{
					$getBooks = showBooks($genre);
				}
				?>

				<?php 
					while($book = $getBooks->fetch())
					{
				?>

				<aside>
					<div class="price"><?= $book['price'] . ' €'; ?></div>

					<?php
						echo '<a href="book.php?id=' .$book['id'] . '"><img src="public/img/books/' . $book['img'] . '" title="' . $book['name'] . '"><br>' . $book['name'] . '</a>';
					?>
				</aside>
				<?php
					}
				?>
			
			</article>
		</main>

		<footer></footer>

	<content>

</body>
</html>