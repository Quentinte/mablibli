<?php 
include('models/model.php'); 
include('class/User.php'); 
include('class/Book.php'); 
include('class/Order.php'); 
session_start(); 

	if(isset($_SESSION['user']))
	{
		$user = $_SESSION['user'];
	}
	else
	{	
		$user = new User(); 
	}
	
	if(isset($_SESSION['order']))
	{
		$order = $_SESSION['order'];
	}
	else
	{
		$order = new Order();
	}
	if(isset($_GET['id']) AND $_GET['id'] > 0)
	{
		$id = htmlspecialchars($_GET['id']);
		$book = new Book($id);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Mablibli : livres en ligne !</title>
	<link rel="stylesheet" type="text/css" href="views/css/index.css">
	<link rel="stylesheet" type="text/css" href="views/css/book.css">
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
</head>
<body>

	<content>

		<header>
			<h1><a href="index.php">Mablibli</a></h1>

			<table>
				<tr>
					<form>
					<td><input id="searchText" type="text"></td><td><input id="searchBtn" type="submit" value="Rechercher"></td>
					</form>
				</tr>
			</table>
			
			<nav>

				<?php include('views/include/menu.php'); ?>

			</nav>
		</header>

		<main>

			<article>
			<?php
				if($book->getId())
				{
			?>
			<aside>
				<?php
					echo '<a href="book.php?id=' .$book->getId() . '"><img src="public/img/books/' . $book->getImage() . '" title="' . $book->getName() . '"></a>';
				?>
				
			</aside>

			<aside>
				<table>
					<tr><td><?= $book->getName() .  ' (' . $book->getDate() . ')'; ?></td></tr>
					<tr><td style="font-size: 0.8em;"><?= $book->getAuthors(); ?></td></tr>
					<tr><td style="padding-top: 10px;"><?= $book->getDescription(); ?></td></tr>
				</table>
			</aside>

			<aside>
				<table>
					<form method="POST" action="book.php?id=<?= $book->getId(); ?>">

					<tr>
						<td><input type="number" min="1" max="10" value="1" name="quantity_order"></td>
						<td><input type="submit" value="Ajouter" name="SUBMIT_ORDER"></td>
					</tr>

					</form>
				</table>
				<?php

					if(isset($_POST['quantity_order']) AND $_POST['quantity_order'] <= 10)
					{
							$_SESSION['order'] = $order;
							$order->addBook($book, $_POST['quantity_order']);

							echo '<div id="ajout"><p>Ajouté au panier</p></div>';
					}
				?>
			</aside>

			<?php
			}
			else
				{
					header('Location: index.php');
					exit();
				}
				?>
			</article>
		</main>

		<footer></footer>

	<content>

</body>
</html>

<?php
}
else{
	header('Location: index.php');
	exit();
}